# Google Docs

Versão web app não oficial do Google Docs, desenvolvido em Electron.

## Compilação:

Linux:

```bash
npm install
npm install electron-packager --save-dev
npm run build-linux
```

## Usage

```bash
npm install
npm electron .
```

## Contribuindo
Qualquer contribuição é bem-vinda.
Crie um issue com suas ideias e discuta a possibilidade de implementá-las. Sempre faça os devidos testes antes de submeter as melhorias.

## Licença
GPL
