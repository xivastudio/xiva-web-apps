# Empacotamento Webapp

Maneira simples de empacotar um Webapp.


## Estrutura Base
    
        DEBIAN
        |
        |___ ¹control | arquivo de controle com nome etc etc etc
        |
        |___ postinst | script a rodar depois de instalar
        |
        |___ postrm | script a rodar depois de desinstalar
        |
        |___ preinst | script a rodar antes de instalar
        |
        |___ prerm | script a rodar antes de desinstalar
        
        • opt/nomedoweapp | contém o binário do webapp
        • usr/share/applications | contém o .desktop
        • usr/share/icons/hicolor/scalable/apps | contém o ícone (de preferência em .svg) do webapp

## Empacotamento para teste local

## Permissões:

Na pasta do webapp:
```bash
chmod 755 DEBIAN
find DEBIAN -name "p*" -exec chmod 555 {} +
```

Na pasta Package:
```bash
dpkg -b nomedapasta-do-webapp
```
Instalar:
```bash
sudo apt install ./nomedoweapp.deb
```

## Estrutura do ¹DEBIAN/control

     • Package: nome do webapp (minúsculo)
     • Version: versão do webapp
     • Architecture: amd64
     • Maintainer: seunome <seu@email.com>
     • Depends: dependências (separadas por vírgula)
     • Conflicts: conflita com (separadas por vírgula)
     • Recommends: pacotes recomendados para serem instalados (separados por vírgula)
     • Section: sessão do webapp podendo elas ser:
        admin, cli-mono, comm, database, debug, devel, doc, editors, education, electronics, embedded, fonts, games, gnome, gnu-r, gnustep, graphics, hamradio, haskell, httpd, interpreters, introspection, java, javascript, kde, kernel, libdevel, libs, lisp, localization, mail, math, metapackages, misc, net, news, ocaml, oldlibs, otherosfs, perl, php, python, ruby, rust, science, shells, sound, tasks, tex, text, utils, vcs, video, web, x11, xfce, zope
     • Priority: prioridade do projeto, podendo ser:
        required, important, standard, optional, extra
     • Homepage: http://xivastudio.org
     • Description: descrição do webapp, devendo terminar em ponto e ter uma linha vazia depois.

Para mais informações:
https://www.debian.org/doc/debian-policy/ch-archive.html#s-subsections

## Contribuindo
Qualquer contribuição é bem-vinda.
Crie um issue com suas ideias e discuta a possibilidade de implementá-las. Sempre faça os devidos testes antes de submeter as melhorias.

## Licença
GPL
